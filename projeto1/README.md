Esse programa recebe um diagrama de sudoku 9x9 e permite realizar algumas operações com ele usando uma abordagem multithread.
É possível fazer verificação se o diagrama está preenchido corretamente; solicitar dicas de possíveis valores para cada célula; e solicitar uma possível solução.

A estratégia adotada foi de dividir o processamento das linhas, colunas e blocos 3x3 em 3 threads. Isso é feito tanto para a verificação quanto para o modo de dica. O modo de solução roda o modo de dica, resolve todos as células com valor único possível e depois resolve as demais (se sobraram) por backtracking usando os possíveis valores calculados.
A performance das operações é alta, mas o programa tem algumas limitações. No caso de diagramas incompletos, não é feita a verificação se os valores já preenchidos são válidos. Também a etapa de solução não usou diretamente multithread (apenas indiretamente pelas dicas), pois o número máximo de threads podia ser facilmente estourado para diagramas mais difíceis.

Ao abrir o programa, o usuário precisa inserir um diagrama. Ele pode estar completo (no caso de verificação) ou incompleto (para as outras operações).

Exemplo de diagrama completo:
8 6 1 3 4 7 2 9 5
4 3 2 8 9 5 1 6 7
9 5 9 1 6 2 4 8 3
2 7 8 4 5 1 6 3 9
5 4 9 6 8 3 7 2 1
6 1 3 2 7 9 8 5 4
3 2 4 9 1 8 5 7 6
1 8 5 7 3 6 9 4 2
7 9 6 5 2 4 3 1 8

Exemplo de diagrama incompleto:
X 6 1 X X X X 9 X
4 3 X X 9 5 X 6 X
9 X X 1 X 2 X X 3
X X X 4 X 1 X X 9
5 X 9 X X X 7 X 1
6 X X 2 X 9 X X X
3 X X 9 X 8 X X 6
X 8 X 7 3 X X 4 2
X 9 X X X X 3 1 X

Depois o usuário é solicitado para escolher a operação. Depois do processamento, o usuário recebe o resultado da operação.