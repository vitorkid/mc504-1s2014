/**
 * Reads an Sudoku 9x9 matrix and performs operations with it.
 * Operations:
 *   - Verification of an complete game.
 *   - Hints for an incomplete game.
 *   - Possible solution for an incomplete game.
 */

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#define SIZE 9
#define NUM_THREADS 3
#define UNASSIGNED 0

struct sudoku_cell {
  int value; // Unique value (when there's only one possibility).
  int p_values[SIZE]; // Vector of "bool" (0/1) indication possible numbers for this cell.
};

typedef struct {
  struct sudoku_cell m[SIZE][SIZE];
} sudoku_matrix;

typedef struct {
  int x, y;
} coordinates;

void zero_matrix(sudoku_matrix* sudoku);
void read_matrix(sudoku_matrix* sudoku);
void print_matrix(sudoku_matrix* sudoku);

void verify_matrix(sudoku_matrix* sudoku);
void* verify_lines(void *s);
void* verify_columns(void *s);
void* verify_squares(void *s);
void verify_squares_aux(sudoku_matrix* sudoku, int i_max, int j_max, int s);

void search_matrix(sudoku_matrix* sudoku);
void* search_lines(void *s);
void* search_columns(void *s);
void* search_squares(void *s);
void search_squares_aux(sudoku_matrix* sudoku, int i_max, int j_max, int s);

int set_cell_value(sudoku_matrix* sudoku, int x, int y, int value);
int check_cell_p_values(sudoku_matrix* sudoku, int x, int y);
int check_all_p_values(sudoku_matrix* sudoku);
coordinates find_unassigned_cell();
int solve_matrix();

int main(int argc, char *argv[]) {
  int i, j, choice;
  sudoku_matrix sudoku;
  
  printf("Insira a o diagrama 9x9.\n");
  read_matrix(&sudoku);
  //print_matrix(&sudoku);
  printf("\n");
  
  while (1) {
    printf("Qual módulo deseja utilizar?\n");
    printf("0 - Verificação\n");
    printf("1 - Modo dica\n");
    printf("2 - Solução\n");
    printf("3 - Sair\n");
    
    scanf("%d", &choice);
    
    switch (choice) {
      case 0:
        verify_matrix(&sudoku);
        return 0;
      case 1:
        search_matrix(&sudoku);
        print_matrix(&sudoku);
        return 0;
      case 2:
        search_matrix(&sudoku);
        if (check_all_p_values(&sudoku) && solve_matrix(&sudoku)) {
          print_matrix(&sudoku);
        }
        else {
          printf("Nenhuma solução é possível para esse diagrama.\n");
        }
        return 0;
      case 3:
        return 0;
      default:
        printf("\nEntrada inválida.\n");
    }
  }
  return 0;
}

/**
 * Initializes all values with zero.
 */
void zero_matrix(sudoku_matrix* sudoku) {
  int i, j, k;
  
  for (i = 0; i < SIZE; i++) {
    for (j = 0; j < SIZE; j++) {
      sudoku->m[i][j].value = UNASSIGNED;
      for (k = 0; k < SIZE; k++) {
        sudoku->m[i][j].p_values[k] = 0;
      }
    }
  }
}

/**
 * Reads the matrix from user input.
 */
void read_matrix(sudoku_matrix* sudoku) {
  int i, j, k, n;
  char cell;
  
  zero_matrix(sudoku);
  
  for (i = 0; i < SIZE; i++) {
    for (j = 0; j < SIZE; j++) {
      scanf(" %c", &cell);
      if (cell == 'X') {
        for (k = 0; k < SIZE; k++) {
          sudoku->m[i][j].p_values[k] = 1;
        }
      }
      else {
        n = cell - '0';
        sudoku->m[i][j].value = n;
      }
    }
  }
}

/**
 * Prints the current state of the matrix.
 * If the cell has more than one possible value, all values are enclosed within parentheses.
 */
void print_matrix(sudoku_matrix* sudoku) {
  int i, j, k;
  
  for (i = 0; i < SIZE; i++) {
    printf("\n");
    for (j = 0; j < SIZE; j++) {
      if (sudoku->m[i][j].value != UNASSIGNED) {
        printf("%d ", sudoku->m[i][j].value);
      }
      else {
        printf("(");
        for (k = 0; k < SIZE; k++) {
          if (sudoku->m[i][j].p_values[k] == 1) {
            printf("%d", k + 1);
          }
        }
        printf(") ");
      }
    }
  }
  printf("\n\n");
}

/**
 * Creates three threads to verify the lines, columns and squares.
 */
void verify_matrix(sudoku_matrix* sudoku) {
  pthread_t threads[NUM_THREADS];
  
  pthread_create(&threads[0], NULL, verify_lines, (void *)sudoku);
  pthread_create(&threads[1], NULL, verify_columns, (void *)sudoku);
  pthread_create(&threads[2], NULL, verify_squares, (void *)sudoku);
  
  pthread_join(threads[0], NULL);
  pthread_join(threads[1], NULL);
  pthread_join(threads[2], NULL);
}

/**
 * Thread that verifies if the lines are corrected.
 * If an error is found, more details are printed.
 */
void* verify_lines(void *s) {
  sudoku_matrix *s_data;
  s_data = (sudoku_matrix *) s;
  int i, j, k, c_atual, c_count;
  int all_ok = 1;
  
  for (i = 0; i < SIZE; i++) {
    for (k = 0; k < SIZE; k++) {
      c_atual = k + 1;
      c_count = 0;
      for (j = 0; j < SIZE; j++) {
        if (s_data->m[i][j].value == c_atual) {
          c_count++;
          if (c_count > 1) {
            printf("%d está repetido na linha %d.\n", c_atual, i + 1);
            all_ok = 0;
            break;
          }
        }
      }
    }
  }
  if (all_ok) {
    printf("Linhas estão OK.\n");
  }
  return NULL;
}

/**
 * Thread that verifies if the columns are corrected.
 * If an error is found, more details are printed.
 */
void* verify_columns(void *s) {
  sudoku_matrix *s_data;
  s_data = (sudoku_matrix *) s;
  int i, j, k, c_atual, c_count;
  int all_ok = 1;
  
  for (j = 0; j < SIZE; j++) {
    for (k = 0; k < SIZE; k++) {
      c_atual = k + 1;
      c_count = 0;
      for (i = 0; i < SIZE; i++) {
        if (s_data->m[i][j].value == c_atual) {
          c_count++;
          if (c_count > 1) {
            printf("%d está repetido na coluna %d.\n", c_atual, j + 1);
            all_ok = 0;
            break;
          }
        }
      }
    }
  }
  if (all_ok) {
    printf("Colunas estão OK.\n");
  }
  return NULL;
}

/**
 * Thread that verifies if the squares are corrected.
 * If an error is found, more details are printed.
 */
void* verify_squares(void *s) {
  sudoku_matrix *s_data;
  s_data = (sudoku_matrix *) s;
  
  verify_squares_aux(s_data, 3, 3, 1);
  verify_squares_aux(s_data, 3, 6, 2);
  verify_squares_aux(s_data, 3, 9, 3);
  verify_squares_aux(s_data, 6, 3, 4);
  verify_squares_aux(s_data, 6, 6, 5);
  verify_squares_aux(s_data, 6, 9, 6);
  verify_squares_aux(s_data, 9, 3, 7);
  verify_squares_aux(s_data, 9, 6, 8);
  verify_squares_aux(s_data, 9, 9, 9);
  
  return NULL;
}

/**
 * Auxiliary function to verify just one square.
 * See verify_squares.
 */
void verify_squares_aux(sudoku_matrix* sudoku, int i_max, int j_max, int s) {
  int i, j, k, l, c_atual;
  
  for (i = i_max - 3; i < i_max; i++) {
    for (j = j_max - 3; j < j_max; j++) {
      c_atual = sudoku->m[i][j].value;
      for (k = i_max - 3; k < i_max; k++) {
        for (l = j_max - 3; l < j_max; l++) {
          if (k == i && l == j)
            continue;
          if (sudoku->m[k][l].value == c_atual) {
            printf("%d está repetido no quadrado %d.\n", c_atual, s);
            return;
          }
        }
      }
    }
  }
  printf("Quadrado %d está OK.\n", s);
}

/**
 * Creates three threads to search for possibles values in each cell for the lines, columns and squares.
 */
void search_matrix(sudoku_matrix* sudoku) {
  pthread_t threads[NUM_THREADS];
  
  pthread_create(&threads[0], NULL, search_lines, (void *)sudoku);
  pthread_create(&threads[1], NULL, search_columns, (void *)sudoku);
  pthread_create(&threads[2], NULL, search_squares, (void *)sudoku);
  
  pthread_join(threads[0], NULL);
  pthread_join(threads[1], NULL);
  pthread_join(threads[2], NULL);
}

/**
 * Thread that searches for possible values considering only lines.
 * If a cell with only one value is found, the value is discarted for the other cells.
 */
void *search_lines(void *s) {
  sudoku_matrix *s_data;
  s_data = (sudoku_matrix *) s;
  int i, j, k, c_atual;
  
  for (i = 0; i < SIZE; i++) {
    for (j = 0; j < SIZE; j++) {
      if (s_data->m[i][j].value != UNASSIGNED) {
        c_atual = s_data->m[i][j].value - 1;
        for (k = 0; k < SIZE; k++) {
          s_data->m[i][k].p_values[c_atual] = 0;
        }
      }
    }
  }
  
  return NULL;
}

/**
 * Thread that searches for possible values considering only columns.
 * If a cell with only one value is found, the value is discarted for the other cells.
 */
void *search_columns(void *s) {
  sudoku_matrix *s_data;
  s_data = (sudoku_matrix *) s;
  int i, j, k, l, c_atual;
  
  for (j = 0; j < SIZE; j++) {
    for (i = 0; i < SIZE; i++) {
      if (s_data->m[i][j].value != UNASSIGNED) {
        c_atual = s_data->m[i][j].value - 1;
        for (k = 0; k < SIZE; k++) {
          s_data->m[k][j].p_values[c_atual] = 0;
        }
      }
    }
  }
  
  return NULL;
}

/**
 * Thread that searches for possible values considering only squares.
 * If a cell with only one value is found, the value is discarted for the other cells.
 */
void *search_squares(void *s) {
  sudoku_matrix *s_data;
  s_data = (sudoku_matrix *) s;
  
  search_squares_aux(s_data, 3, 3, 1);
  search_squares_aux(s_data, 3, 6, 2);
  search_squares_aux(s_data, 3, 9, 3);
  search_squares_aux(s_data, 6, 3, 4);
  search_squares_aux(s_data, 6, 6, 5);
  search_squares_aux(s_data, 6, 9, 6);
  search_squares_aux(s_data, 9, 3, 7);
  search_squares_aux(s_data, 9, 6, 8);
  search_squares_aux(s_data, 9, 9, 9);
  
  return NULL;
}

/**
 * Auxiliary function to search for possible values for just one square.
 * See search_squares.
 */
void search_squares_aux(sudoku_matrix* sudoku, int i_max, int j_max, int s) {
  int i, j, k, l, c_atual;
  
  for (i = i_max - 3; i < i_max; i++) {
    for (j = j_max - 3; j < j_max; j++) {
      if (sudoku->m[i][j].value != UNASSIGNED) {
        c_atual = sudoku->m[i][j].value - 1;
        for (k = i_max - 3; k < i_max; k++) {
          for (l = j_max - 3; l < j_max; l++) {
            sudoku->m[k][l].p_values[c_atual] = 0;
          }
        }
      }
    }
  }
}

/**
 * Sets the value to a cell and removes it from p_values of others cell from its line, column and square.
 * Returns 1 if it the others cells are okay with it, otherwise returns 0;
 */
int set_cell_value(sudoku_matrix* sudoku, int x, int y, int value) {
  int i, j, i_max, j_max;
  i_max = ((x + 3) / 3) * 3;
  j_max = ((y + 3) / 3) * 3;
  sudoku->m[x][y].value = value;
  
  // Sets other p_values to 0;
  for (i = 0; i < SIZE; i++) {
    sudoku->m[i][y].p_values[value - 1] = 0;
  }
  for (j = 0; j < SIZE; j++) {
    sudoku->m[x][j].p_values[value - 1] = 0;
  }
  for (i = i_max - 3; i < i_max; i++) {
    for (j = j_max - 3; j < j_max; j++) {
      sudoku->m[i][j].p_values[value - 1] = 0;
    }
  }
  
  // Checks if something went wrong.
  for (i = 0; i < SIZE; i++) {
    if (i != x && !check_cell_p_values(sudoku, i, y))
      return 0;
  }
  for (j = 0; j < SIZE; j++) {
    if (j != y && !check_cell_p_values(sudoku, x, j))
      return 0;
  }
  for (i = i_max - 3; i < i_max; i++) {
    for (j = j_max - 3; j < j_max; j++) {
      if (i != x && j != y && !check_cell_p_values(sudoku, i, j))
        return 0;
    }
  }
  
  return 1;
}

/**
 * Checks if this cell has a value or possible values.
 * If there's only one possible value, the value will be set.
 * Returns 1 if cell has value/p_value, otherwise returns 0.
 */
int check_cell_p_values(sudoku_matrix* sudoku, int x, int y) {
  int i, value, count = 0;

  if (sudoku->m[x][y].value != UNASSIGNED) {
    return 1;
  }
  
  for (i = 0; i < SIZE; i++) {
    if (sudoku->m[x][y].p_values[i] == 1) {
      value = i + 1;
      count++;
    }
  }
  
  if (count == 1) {
    return set_cell_value(sudoku, x, y, value);
  }
  else if (count > 1) {
    return 1;
  }
  
  return 0;
}

/**
 * Uses check_cell_p_values to check if all cells have value or p_values.
 * If there's only one possible value for a cell, the value will be set.
 * Returns 1 if all cells have value/p_value, otherwise returns 0.
 */
int check_all_p_values(sudoku_matrix* sudoku) {
  int i, j;
  
  for (i = 0; i < SIZE; i++) {
    for (j = 0; j < SIZE; j++) {
      if (!check_cell_p_values(sudoku, i, j)) {
        return 0;
      }
    }
  }
  return 1;
}

/**
 * Auxiliary function to find a cell with multiple possible values.
 * Returns the index (-1,-1) if none is found.
 */
coordinates find_unassigned_cell(sudoku_matrix* sudoku) {
  coordinates c;
  int i, j;
  
  for (i = 0; i < SIZE; i++) {
    for (j = 0; j < SIZE; j++) {
      if (sudoku->m[i][j].value == UNASSIGNED) {
        c.x = i;
        c.y = j;
        return c;
      }
    }
  }
  c.x = c.y = -1;
  
  return c;
}

/**
 * Uses backtracking to find a possible solution considering the possible values given.
 * Returns 1 if a solution is found, otherwise returns 0.
 */
int solve_matrix(sudoku_matrix* sudoku) {
  coordinates c = find_unassigned_cell(sudoku);
  sudoku_matrix sudoku_copy = *sudoku;
  int i;
  
  if (c.x == -1 && c.y == -1)
    return 1;
  
  for (i = 0; i < SIZE; ++i) {
    if (sudoku->m[c.x][c.y].p_values[i] == 1) {
      if (set_cell_value(sudoku, c.x, c.y, i + 1)) {
        if (solve_matrix(sudoku))
          return 1;
      }
      *sudoku = sudoku_copy;
    }
  }
  
  return 0;
}