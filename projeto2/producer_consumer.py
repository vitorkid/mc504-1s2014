import time
import random
import queue
from threading import Thread, Lock
from ncurses import * #import all functions from ncurses.py file

# Prints a message and try to read a positive integer value.
# Returns input value if successful, otherwise returns default.
def read_int_input(message, default):
    try:
        value = input(message)
        int_value = int(value)
        return int_value if int_value > 0 else default
    except ValueError:
        return default

# Reading user input.
N_PRODUCERS = read_int_input('Enter number of producers (default to 4): ', 4)
N_CONSUMERS = read_int_input('Enter number of consumers (default to 3): ', 3)
MAX_PRODUCTS = read_int_input('Enter max number of products (default to 12): ', 12)
PRODUCT_TYPES = range(1, read_int_input('Enter number of product types (default to 3): ', 3) + 1)

# Creating products queue.
products_queue = queue.Queue(MAX_PRODUCTS)
animation_lock = Lock()

# Initializing ncurses
initialize(N_PRODUCERS, N_CONSUMERS, PRODUCT_TYPES, products_queue)

# Producer thread class with unique id.
class ProducerThread(Thread):
    producers_count = 0
    
    def __init__(self):
        ProducerThread.producers_count += 1
        self.id = ProducerThread.producers_count
        super(ProducerThread, self).__init__()
    
    def run(self):
        global products_queue
        time.sleep(random.random()) #preventing starvation of other threads
        while True:
            product_type = random.choice(PRODUCT_TYPES) #producer makes any product of the range
            with animation_lock: #gains control of the animation process
                try:
                    products_queue.put_nowait(product_type) #tries to put in the queue, otherwise throws exception
                    produce(self.id, product_type)
                except queue.Full:
                    pass #repeats the process
            time.sleep(random.random()) #again, prevents starvation

# Consumer thread class with unique id.
class ConsumerThread(Thread):
    consumers_count = 0
    
    def __init__(self):
        ConsumerThread.consumers_count += 1
        self.id = ConsumerThread.consumers_count
        super(ConsumerThread, self).__init__()
    
    def run(self):
        global products_queue
        time.sleep(random.random()) #preventing starvation of other threads
        while True:
            with animation_lock: #gains control of the animation process
                try:
                    product_type = products_queue.get_nowait() #tries to get from queue, otherwise throws exception
                    consume(self.id, product_type)
                    products_queue.task_done()
                except queue.Empty:
                    pass #repeats the process
            time.sleep(random.random()) #again, prevents starvation

# Creating and starting producers and consumer threads.
producers = [ProducerThread() for i in range(N_PRODUCERS)]
for producer in producers: producer.start()
consumers = [ConsumerThread() for i in range(N_CONSUMERS)]
for consumer in consumers: consumer.start()

close_curse() # let user close curses
