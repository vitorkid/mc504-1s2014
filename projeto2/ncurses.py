import curses
import time

def initialize(prod, cons, types, queue):
    global N_PRODUCERS
    N_PRODUCERS = prod
    global N_CONSUMERS
    N_CONSUMERS = cons
    global PRODUCT_TYPES
    PRODUCT_TYPES = types
    global products_queue
    products_queue = queue
    global screen
    screen = curses.initscr() # starting ncurses mode

def close_curse():
    screen.getch() #waits for a key to be pressed,
    curses.endwin() #ends the program
    
def produce(p_index, type):
    dim = screen.getmaxyx() # getmaxyx() returns shell screen limits
    
    #for j in range(N_PRODUCERS):
    for z in range(6):
        screen.clear()
        screen.addstr(2,dim[1]//2-21, "Producers")
        screen.addstr(2,dim[1]//2-4,"Shelves")
        screen.addstr(2,dim[1]//2+12, "Consumers")
        # adiciona os produtores na tela
        for i in range(N_PRODUCERS):
            screen.addstr((dim[0]//2-N_PRODUCERS//2+1)+i, dim[1]//2-18, '\o/')
        # faz os produtores caminharem ate o shelf, um por vez
        screen.addstr((dim[0]//2-N_PRODUCERS//2+1)+p_index-1,dim[1]//2-18+z,'\o/')
        # retira atomicidade dos 3 primeiros movimentos do boneco
        if z == 1: screen.addstr((dim[0]//2-N_PRODUCERS//2+1)+p_index-1,dim[1]//2-18, ' ')
        if z == 2: screen.addstr((dim[0]//2-N_PRODUCERS//2+1)+p_index-1,dim[1]//2-18, '  ')
        if z >= 3: screen.addstr((dim[0]//2-N_PRODUCERS//2+1)+p_index-1,dim[1]//2-18, '   ')
        for i in range(N_CONSUMERS):
            screen.addstr((dim[0]//2-N_CONSUMERS//2+1)+i, dim[1]//2+15, '_o_')
        # adicionando shelf na tela
        if z == 5: print_shelves(dim, type, 0) # se chegou na shelf
        else: print_shelves(dim, type, -1) # se não chegou ainda
        screen.refresh()
        time.sleep(0.2)
        
def consume(c_index, type):
    dim = screen.getmaxyx() # getmaxyx() returns shell screen limits
    
    #for j in range(N_CONSUMERS):
    for z in range((dim[1])//2+15,dim[1]//2+9,-1):
        screen.clear()
        screen.addstr(2,dim[1]//2-21, "Producers")
        screen.addstr(2,dim[1]//2-4,"Shelves")
        screen.addstr(2,dim[1]//2+12, "Consumers")
        # adiciona os consumidores na tela
        for i in range(N_CONSUMERS):
            screen.addstr((dim[0]//2-N_CONSUMERS//2+1)+i, dim[1]//2+15, '_o_')
        # faz os consumidores caminharem ate o shelf, um por vez
        screen.addstr((dim[0]//2-N_CONSUMERS//2+1)+c_index-1, z,'_o_')
        # retira atomicidade dos 3 primeiros movimentos do boneco
        if z == dim[1]//2+14: screen.addstr((dim[0]//2-N_CONSUMERS//2+1)+c_index-1,dim[1]//2+17, ' ')
        if z == dim[1]//2+13: screen.addstr((dim[0]//2-N_CONSUMERS//2+1)+c_index-1,dim[1]//2+16, '  ')
        if z <= dim[1]//2+12: screen.addstr((dim[0]//2-N_CONSUMERS//2+1)+c_index-1,dim[1]//2+15, '   ')
        for i in range(N_PRODUCERS):
            screen.addstr((dim[0]//2-N_PRODUCERS//2+1)+i, dim[1]//2-18, '\o/')
        # adicionando shelf na tela
        if z == (dim[1]//2)+10: print_shelves(dim, type, 0) # se chegou na shelf
        else: print_shelves(dim, type, 1) # se não chegou ainda
        screen.refresh()
        time.sleep(0.2)

def print_shelves(dim, type, aux):
    types_count = [0 for type in PRODUCT_TYPES]
    for item in products_queue.queue:
        types_count[item-1] += 1
    
    y = (dim[0]//2)-len(PRODUCT_TYPES)//2
    screen.addstr(y-1, (dim[1]//2)-10, '____________________')
    screen.addstr(y, (dim[1]//2)-10, '|                  |')
    for p_type in PRODUCT_TYPES:
        y += 1
        count = types_count[p_type-1] if p_type != type else types_count[p_type-1] + aux
        shelf_text = "| Product #" + "{:<3d}".format(p_type)
        shelf_text += "{:<5}".format("("+ str(count) + ")") + "|"
        screen.addstr(y, (dim[1]//2)-10, shelf_text)
    screen.addstr(y+1, (dim[1]//2)-10, '|__________________|')